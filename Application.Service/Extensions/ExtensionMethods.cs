﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Application.Services.Extensions
{
    public static class ExtensionMethods
    {
        public static object Sum(this IQueryable data, string propertyName)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            PropertyInfo property = data.ElementType.GetProperty(propertyName);
            if (property == null)
            {
                throw new ArgumentException(nameof(propertyName));
            }

            ParameterExpression parameter = Expression.Parameter(data.ElementType, "s");
            MemberExpression getter = Expression.MakeMemberAccess(parameter, property);
            Expression selector = Expression.Lambda(getter, parameter);

            // Get Sum Method by property type
            MethodInfo method = typeof(Queryable).GetMethods().First(
                m => m.Name == "Sum"
                     && m.ReturnType == property.PropertyType
                     && m.IsGenericMethod);
            // Make generic
            var genericMethod = method.MakeGenericMethod(data.ElementType);

            var callExpression = Expression.Call(
                null,
                genericMethod,
                new[] { data.Expression, Expression.Quote(selector) });

            return data.Provider.Execute(callExpression);
        }

        public static object Average(this IQueryable data, string propertyName)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }

            PropertyInfo property = data.ElementType.GetProperty(propertyName);
            if (property == null)
            {
                throw new ArgumentException(nameof(propertyName));
            }

            ParameterExpression parameter = Expression.Parameter(data.ElementType, "s");
            MemberExpression getter = Expression.MakeMemberAccess(parameter, property);
            Expression selector = Expression.Lambda(getter, parameter);


            // Get Average Method by property type
            MethodInfo method = typeof(Queryable).GetMethods().Last(
                m => m.Name == "Average"
                     && m.ReturnType == property.PropertyType
                     && m.IsGenericMethod);
            // Make generic
            var genericMethod = method.MakeGenericMethod(data.ElementType);

            var callExpression = Expression.Call(
                null,
                genericMethod,
                new[] { data.Expression, Expression.Quote(selector) });

            return data.Provider.Execute(callExpression);
        }
    }
}
