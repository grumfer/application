﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using static System.Linq.Expressions.Expression;
using Application.Services.Extensions;
using Application.Services.Abstraction;

namespace Application.Services
{
    public class AggregationService : IAggregationService
    {
        public IEnumerable<T> Aggregate<T>(IEnumerable<T> data, string groupByColumn,
            string sumColumn = null, string averageColumn = null) where T: new()
        {
            if (data == null || !data.Any() || string.IsNullOrEmpty(groupByColumn))
            {
                throw new ArgumentNullException(nameof(data) + " or " + nameof(groupByColumn));
            }

            Type type = typeof(T);
            PropertyInfo[] properties =
                type.GetProperties()
                    .Where(x => x.PropertyType == typeof(string))
                    .ToArray();

            // if no properties in generic type or no groupByColumn property exist
            if (!properties.Any() || properties.All(p => p.Name != groupByColumn))
            {
                throw new ArgumentException(nameof(groupByColumn));

            }

            var grouped = data.AsQueryable().GroupBy(GetGroupKey<T>(groupByColumn)).ToList();

            foreach (var group in grouped)
            {
                var def = group.FirstOrDefault();
                if (def == null)
                {
                    continue;
                }

                var defProp = def.GetType().GetProperties().Where(p => !p.Name.Contains(groupByColumn)).ToList();

                if (sumColumn != null)
                {
                    var sumProp = def.GetType().GetProperty(sumColumn!, BindingFlags.Public | BindingFlags.Instance);
                    if (sumProp != null && sumProp.CanWrite)
                    {
                        var sum = group.AsQueryable().Sum(sumColumn);
                        sumProp.SetValue(def, sum, null);
                    }

                    defProp.RemoveAll(p => p.Name.Contains(sumColumn));
                }

                if (averageColumn != null)
                {
                    var averageProp = def.GetType()
                        .GetProperty(averageColumn, BindingFlags.Public | BindingFlags.Instance);
                    if (averageProp != null && averageProp.CanWrite)
                    {
                        var average = group.AsQueryable().Average(averageColumn);
                        averageProp.SetValue(def, average, null);
                    }
                    defProp.RemoveAll(p => p.Name.Contains(averageColumn));
                }

                // Set default values
                foreach (var propertyInfo in defProp.ToList())
                {
                    propertyInfo.SetValue(def, null);
                }
            }
            
            return grouped.Select(g => g.First());
        }

        private static Expression<Func<T, string>> GetGroupKey<T>(string property)
        {
            var parameter = Parameter(typeof(T));
            var body = Property(parameter, property);
            return Lambda<Func<T, string>>(body, parameter);
        }
    }
}
