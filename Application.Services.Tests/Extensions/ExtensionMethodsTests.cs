﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Domain;
using Application.Services.Extensions;
using Xunit;

namespace Application.Services.Tests.Extensions
{
    public class ExtensionMethodsTests
    {
        private readonly List<Symbol> _moqTestList = new()
        {
            new Symbol
            {
                Price = 1,
                Quantity = 1
            },
            new Symbol
            {
                Price = 2,
                Quantity = 2
            },
            new Symbol
            {
                Price = 3,
                Quantity = 3
            }
        };

        [Fact]
        public void SumThrowExceptionOnNullData()
        {
            List<Symbol> moqList = null;
            Assert.Throws<ArgumentNullException>(() => moqList.AsQueryable().Sum("Name"));
        }

        [Fact]
        public void SumThrowExceptionOnNullColumnName()
        {
            Assert.Throws<ArgumentNullException>(() => _moqTestList.AsQueryable().Sum(null));
        }

        [Fact]
        public void SumThrowExceptionOnWrongColumnName()
        {
            Assert.Throws<ArgumentException>(() => _moqTestList.AsQueryable().Sum("SomeName"));
        }

        [Fact]
        public void AverageThrowExceptionOnNullData()
        {
            List<Symbol> moqList = null;
            Assert.Throws<ArgumentNullException>(() => moqList.AsQueryable().Average("Name"));
        }

        [Fact]
        public void AverageThrowExceptionOnNullColumnName()
        {
            Assert.Throws<ArgumentNullException>(() => _moqTestList.AsQueryable().Average(null));
        }

        [Fact]
        public void AverageThrowExceptionOnWrongColumnName()
        {
            Assert.Throws<ArgumentException>(() => _moqTestList.AsQueryable().Average("SomeName"));
        }

        [Theory]
        [InlineData("Price")]
        [InlineData("Quantity")]
        public void SumReturnsCorrectValue(string columnName)
        {
            Assert.Equal(6.0, Convert.ToDouble(_moqTestList.AsQueryable().Sum(columnName)));
        }

        [Theory]
        [InlineData("Price")]
        public void AverageReturnsCorrectValue(string columnName)
        {
            Assert.Equal(2.0, Convert.ToDouble(_moqTestList.AsQueryable().Average(columnName)));
        }
    }
}
