﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Domain;
using Xunit;

namespace Application.Services.Tests
{
    public class AggregationServiceTests
    {
        private List<Symbol> GetMoqData()
        {
            return new()
            {
                new Symbol
                {
                    Id = 1,
                    Name = "EURUSD",
                    Quantity = 2,
                    Price = 1.56
                },
                new Symbol
                {
                    Id = 2,
                    Name = "EURUSD",
                    Quantity = 3,
                    Price = 1.57
                }
            };
        }

        [Fact]
        public void AggregateReturnCorrectValueWithCorrectData()
        {
            var moqData = GetMoqData();

            var moqService = new AggregationService();

            var result = moqService.Aggregate(moqData, "Name", "Quantity", "Price");

            Assert.Equal("EURUSD", result.First().Name);
            Assert.Equal(5, result.First().Quantity);
            Assert.Equal(1.565, result.First().Price);
        }

        [Fact]
        public void AggregateReturnCorrectValueWithNoAverageColumnParam()
        {
            var moqData = GetMoqData();

            var moqService = new AggregationService();

            var result = moqService.Aggregate(moqData, "Name", "Quantity");

            Assert.Equal("EURUSD", result.First().Name);
            Assert.Equal(5, result.First().Quantity);
            Assert.Equal(0.0, result.First().Price);
        }

        [Fact]
        public void AggregateReturnCorrectValueWithNoSumAndAverageColumnParam()
        {
            var moqData = GetMoqData();

            var moqService = new AggregationService();

            var result = moqService.Aggregate(moqData, "Name");

            Assert.Equal("EURUSD", result.First().Name);
            Assert.Equal(0, result.First().Quantity);
            Assert.Equal(0.0, result.First().Price);
        }

        [Fact]
        public void AggregateThrowExceptionWithNullGroupColumn()
        {
            var moqData = GetMoqData();

            var moqService = new AggregationService();

            Assert.Throws<ArgumentNullException>(() => moqService.Aggregate(moqData, null));
        }

        [Fact]
        public void AggregateThrowExceptionWithWrongGroupColumn()
        {
            var moqData = GetMoqData();

            var moqService = new AggregationService();

            Assert.Throws<ArgumentException>(() => moqService.Aggregate(moqData, "Wrong"));
        }

        [Fact]
        public void AggregateThrowExceptionWithData()
        {
            var moqService = new AggregationService();

            Assert.Throws<ArgumentNullException>(() => moqService.Aggregate((List<Symbol>) null, "SomeName"));
        }
    }
}
