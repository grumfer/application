﻿using System.Collections.Generic;

namespace Application.Services.Abstraction
{
    public interface IAggregationService
    {
        IEnumerable<T> Aggregate<T>(IEnumerable<T> data, string groupByColumn, string sumColumn = null, string averageColumn = null) where T : new();
    }
}
