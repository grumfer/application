﻿using System;
using System.Collections.Generic;
using Application.Domain;
using Application.Services;
using Application.Services.Abstraction;
using Microsoft.Extensions.DependencyInjection;


namespace Application
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var bar = BuildAggregationService();

            var allSymbols = DataInitialize();
            var result = bar.Aggregate(allSymbols, "Name", "Quantity", "Price");
            Console.Write(result);
        }

        private static List<Symbol> DataInitialize()
        {
            return new()
            {
                new Symbol
                {
                    Id = 1,
                    Name = "EURUSD",
                    Quantity = 2,
                    Price = 1.56
                },
                new Symbol
                {
                    Id = 2,
                    Name = "EURUSD",
                    Quantity = 3,
                    Price = 1.57
                },
                new Symbol
                {
                    Id = 3,
                    Name = "EURGBP",
                    Quantity = 4,
                    Price = 1.23
                }
            };
        }

        private static IAggregationService BuildAggregationService()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IAggregationService, AggregationService>()
                .BuildServiceProvider();

            return serviceProvider.GetService<IAggregationService>();
        }
    }
}
